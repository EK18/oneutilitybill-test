<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Home page and redirect
Route::get('/', 'HomeController@home');

Route::get('/home', function (Request $request) {
    return redirect('/');
});

// Search Page -- does a few where like commands to pull data relevant to search.
Route::get('search', 'HomeController@search');

// Authentication routes (default to laravel)
Auth::routes();

// Blog collection page
Route::get('/blog', 'PostController@posts');

// Blog posts pulling the post data
Route::get('blog/{path}', 'PostController@post');

// Post Edit page with security
Route::get('blog/{path}/edit', 'PostController@editPost')->middleware('auth');

// Submit new page with security
Route::get('/submit', 'PostController@makePost')->middleware('auth');

//Post requests with validation and security
Route::post('/submit', 'PostController@SubmitStore')->name('post.submit');

Route::post('/update', 'PostController@UpdateStore')->name('post.update')->middleware('auth');

Route::post('/comment/store', 'CommentController@store')->name('comment.add')->middleware('auth');

Route::post('/reply/store', 'CommentController@replyStore')->name('reply.add')->middleware('auth');
