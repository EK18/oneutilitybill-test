@extends('layouts.app')
@section('content')
        <div class="content container rounded bg-white">
          <div class="container">
            @if(isset($result))
                <p> The Search results for your query <b> {{ $search }} </b> are :</p>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>@sortablelink('title')</th>
                        <th>@sortablelink('created_at', 'Creation Date')</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($result as $item)
                    <tr>
                        <td>{{$item->title}}</td>
                        <td>{{$item->created_at}}</td>
                        <td><a href="/blog/{{ $item->uri }}"><button class="btn-unique btn-sm"> View Page </button></a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @endif
            {!! $result->links() !!}
        </div>
        </div>
@endsection
