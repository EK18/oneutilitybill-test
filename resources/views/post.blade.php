@extends('layouts.app')
@section('content')
        <div class="flex-center position-ref full-height">
            <!-- Page content begins -->
            @section('content')
            <div class="container">
                <div class="content bg-white rounded">
                  <h1>{{ $post->title }}</h1></br>
                  <div class="container">
                    <p>{!! $post->content !!}</p>
                  </div>
                </div>
                @auth
                @if(Auth::User()->admin == 1)
                  <div class="container mb-4">
                    <a class="btn-danger btn-unique btn-lg" href="/blog/{{ $post->uri }}/edit">edit</a>
                  </div>
                @endif
                @endauth
                <h4>Comments:</h4>
                <div>
                @include('partials._comment_nested', ['comments' => $post->comments, 'post_id' => $post->id])
              </div>
                @guest
                    <h3>Log in to make comments</h3>
                @else
                <h4>Add comment</h4>
                    <form method="post" action="{{ route('comment.add') }}">
                        @csrf
                        <div class="form-group">
                            <input type="text" name="comment_body" class="form-control" required/>
                            <input type="hidden" name="post_id" value="{{ $post->id }}" />
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-warning" value="Add Comment" />
                        </div>
                    </form>
                @endguest
            </div>
        </div>
@endsection
