@foreach($comments as $comment)
   <div class="ml-4 my-0 p-2 border-left border-dark">
       <strong>User: {{ $comment->user->name }}</strong>
       <p class="bg-white rounded my-0 p-2">{{ $comment->body }}</p>
       @include('partials._comment_nested', ['comments' => $comment->replies])
       @auth
       <a href="" class="my-0" id="reply"></a>
       @if($loop->last)
       <form class="form-group my-0 p-2" method="post" action="{{ route('reply.add') }}">
           @csrf
           <div class="form-group my-0 p-2">
               <input type="text" name="comment_body" class="form-control" required/>
               <input type="hidden" name="post_id" value="{{ $post_id }}" />
               <input type="hidden" name="comment_id" value="{{ $comment->id }}" />
           </div>
           <div class="form-group my-0 p-2">
               <input type="submit" class="btn btn-warning" value="Reply" />
           </div>
       </form>
       @else
       @endif
       @endauth

   </div>
@endforeach
