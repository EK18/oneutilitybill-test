@extends('layouts.app')
@section('content')
        <div class="container align-self-center">
          <div class="content">
              <table class="table table-striped">
                  <thead>
                      <tr>
                          <th>@sortablelink('title')</th>
                          <th>@sortablelink('created_at', 'Creation Date')</th>
                          <th></th>
                      </tr>
                  </thead>
                  <tbody>
                    @foreach ($posts as $post)
                      <tr>
                          <td>{{$post->title}}</td>
                          <td>{{$post->created_at}}</td>
                          <td><a href="/blog/{{ $post->uri }}"><button class="btn-unique btn-sm"> View Page </button></a></td>
                      </tr>
                    @endforeach
                  </tbody>
              </table>
            {!! $posts->links() !!}
          </div>
        </div>

@endsection
