@extends('layouts.app')
@section('content')

        <div class="content container rounded bg-white">
          <div class="container">
            @if(isset($posts))
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>@sortablelink('title')</th>
                        <th>@sortablelink('created_at', 'Creation Date')</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($posts as $item)
                    <tr>
                        <td>{{$item->title}}</td>
                        <td>{{$item->created_at}}</td>
                        <td><a href="/blog/{{ $item->uri }}"><button class="btn-unique btn-sm"> View Page </button></a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @endif
            {!! $posts->links() !!}
          </div>
          <h1 class="title">
            Welcome to the oneutilitybill test blog
          </h1>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Venenatis a condimentum vitae sapien pellentesque habitant morbi tristique. Viverra aliquet eget sit amet tellus cras adipiscing enim. Dui sapien eget mi proin sed libero. Aliquam ut porttitor leo a. Eget est lorem ipsum dolor sit amet consectetur adipiscing. Facilisi etiam dignissim diam quis enim lobortis scelerisque fermentum dui. Scelerisque fermentum dui faucibus in ornare quam. Aliquam sem et tortor consequat id porta nibh venenatis cras. Ut consequat semper viverra nam libero justo laoreet sit.
          </p>
          <p>
            Felis imperdiet proin fermentum leo. Ut tristique et egestas quis ipsum suspendisse. Ornare lectus sit amet est placerat in egestas. Ultrices in iaculis nunc sed augue lacus viverra vitae. Nulla at volutpat diam ut venenatis tellus in metus vulputate. Amet luctus venenatis lectus magna fringilla urna porttitor rhoncus. Placerat orci nulla pellentesque dignissim. Diam sit amet nisl suscipit. Nec nam aliquam sem et tortor consequat id porta nibh. Blandit volutpat maecenas volutpat blandit. Turpis cursus in hac habitasse platea dictumst quisque sagittis. Scelerisque in dictum non consectetur. Urna nunc id cursus metus aliquam. Dui faucibus in ornare quam viverra orci sagittis eu volutpat. Felis bibendum ut tristique et.
          </p>
          <p>
            Gravida dictum fusce ut placerat. Consequat nisl vel pretium lectus quam id leo. Vitae ultricies leo integer malesuada nunc vel risus commodo. Mauris pellentesque pulvinar pellentesque habitant morbi tristique senectus. Commodo ullamcorper a lacus vestibulum sed arcu non odio euismod. At tempor commodo ullamcorper a lacus vestibulum sed arcu non. Pellentesque adipiscing commodo elit at imperdiet dui accumsan. Feugiat vivamus at augue eget arcu. Condimentum mattis pellentesque id nibh tortor id aliquet. Amet cursus sit amet dictum sit amet. Felis imperdiet proin fermentum leo vel orci porta. Luctus accumsan tortor posuere ac ut consequat semper viverra nam. Adipiscing at in tellus integer feugiat scelerisque varius morbi enim. Amet est placerat in egestas. Aenean sed adipiscing diam donec adipiscing. Egestas dui id ornare arcu odio ut. Malesuada pellentesque elit eget gravida. Nunc sed augue lacus viverra. Aliquet eget sit amet tellus.
          </p>
          <p>
          Lectus proin nibh nisl condimentum. Morbi non arcu risus quis varius quam quisque id diam. Lorem sed risus ultricies tristique. Blandit massa enim nec dui nunc mattis enim. Purus non enim praesent elementum facilisis leo vel fringilla. Posuere morbi leo urna molestie at elementum eu facilisis sed. Quisque non tellus orci ac auctor augue mauris augue. Sit amet nisl suscipit adipiscing bibendum est ultricies integer. Enim nunc faucibus a pellentesque sit amet porttitor eget. Massa tempor nec feugiat nisl. Velit scelerisque in dictum non consectetur. Tristique senectus et netus et malesuada fames ac turpis.
          </p>
        </div>
@endsection
