@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="container">
            <h1>Edit your post</h1>
            <form action="/update" method="post">
                @if ($errors->any())
                    <div class="alert alert-danger" role="alert">
                        Please fix the following errors
                    </div>
                @endif

                {!! csrf_field() !!}
                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                    <label for="title">Title</label>
                    <input type="text" class="form-control" id="title" name="title" placeholder="Title" value="{{ $old->title }}">
                    @if($errors->has('title'))
                        <span class="help-block">{{ $errors->first('title') }}</span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('uri') ? ' has-error' : '' }}">
                    <label for="uri">URI</label>
                    <input type="text" class="form-control" id="uri" name="uri" placeholder="URI" value="{{ $old->uri }}">
                    @if($errors->has('uri'))
                        <span class="help-block">{{ $errors->first('uri') }}</span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
                    <label for="content">Content</label>
                    <textarea class="content" id="content" name="content" placeholder="Content">{{ $old->content }}</textarea>
                    @if($errors->has('content'))
                        <span class="help-block">{{ $errors->first('content') }}</span>
                    @endif
                </div>
                <input type="hidden" class="form-control" id="id" name="id" value="{{ $old->id }}">
                <button type="submit" class="btn-sm btn-default">Submit</button>
            </form>
        </div>
    </div>
    <script src="{{ asset('node_modules/tinymce/tinymce.js') }}"></script>
    <script>
        tinymce.init({
            selector:'textarea.content',
        });
    </script>
@endsection
