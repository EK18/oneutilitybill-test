<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Post extends Model
{
  public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable')->whereNull('parent_id');
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    use Sortable;
    protected $fillable = [
        'title',
        'uri',
        'content'
    ];
    public $sortable = ['title', 'created_at'];
}
