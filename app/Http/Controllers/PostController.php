<?php

namespace App\Http\Controllers;

use App;
use Illuminate\Http\Request;
use Auth;

class PostController extends Controller
{
  /**
   * Show all posts in sortable and paginated format.
   */
  public function posts() {
      $posts = \App\Post::sortable()->orderByDesc('created_at')->Paginate(10);

      return view('blogs', ['posts' => $posts]);
  }

  /**
   * Show a posts content, pulls in links to comments
   */
  public function post($path) {
    $post = \App\Post::all()->where('uri', $path)->first();

    return view('post', ['post' => $post]);
  }

  /**
   * Show the make new post page. checks for admin priveleges then loads the content.
   */
  public function makePost() {
    If(Auth::check() && Auth::User()->admin == 1)
    {
      $post = new App\Post();
      return view('submit', ['post' => $post]);
    } else {
      return redirect('/');
    }
  }

  /**
   * Shows edit page after checking for admin priveleges.
   */
  public function editPost($path) {
    If(Auth::check() && Auth::User()->admin == 1)
    {
      $post = \App\Post::all()->where('uri', $path)->first();
      return view('update', ['old' => $post, 'path' => $path]);
    } else {
      return redirect('/');
    }
  }

  /**
   * validates input from form, checks for admin privileges.
   */
  public function SubmitStore(Request $request) {
    If(Auth::check() && Auth::User()->admin == 1)
    {
      $data = $request->validate([
          'title' => 'required|max:255',
          'uri' => 'required|max:255',
          'content' => 'required',
      ]);

      $post = tap(new App\Post($data))->save();
    }
      return redirect('/blog/' . $request->uri);
  }

  /**
   * validates input from form, checks for admin privileges and stores over same ID.
   */
  public function UpdateStore(Request $request) {
    If(Auth::check() && Auth::User()->admin == 1)
    {
      $data = $request->validate([
          'title' => 'required|max:255',
          'uri' => 'required|max:255',
          'content' => 'required',
          'id' => 'required',
      ]);

      $post = App\Post::where('id',$data['id'])->update(['title' => $data['title'],'uri' => $data['uri'],'content' => $data['content']]);
    }

      return redirect('/blog/' . $request->uri);
  }
}
