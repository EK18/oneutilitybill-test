<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
  /**
   * Show the home page. create paginated and sortable variable to store made posts
   */
    public function home() {
        $posts = \App\Post::sortable()->orderByDesc('created_at')->Paginate(6);
        return view('welcome', ['posts' => $posts]);
    }

    /**
     * Search functionality. Takes a basic LIKE statement over title and the URI
     */
    public function search(Request $request) {
      $result = \App\Post::where('title', 'LIKE', "%{ $request->search }%")->orWhere('uri', 'LIKE', "%{$request->search}%")->sortable()->orderByDesc('created_at')->Paginate(10);
      return view('search', ['result' => $result, 'search' => $request->search]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
}
